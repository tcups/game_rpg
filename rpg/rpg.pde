String[] imageNames = {"a.png", "t_kitchentable_br.png", "t_kitchenfloor.png", 
"t_kitchentable_bl.png",  "t_kitchenstool.png", "t_kitchentable_tr.png", "a.png", 
"t_kitchentable_tl.png", "t_kitchenpillow.png", "t_kitchentv_l.png",  "a.png", "t_kitchentv_r.png",
"a.png", "t_brickwall.png","a.png", "t_kitchenplant.png"};
PImage[] tiles = new PImage[imageNames.length];

PVector pLoc = new PVector(6, 2);

//gamestate includes 2Dmaps
int[][][] gameState = {{{13,13,13,13,13,13,13,13,13,13,13},
                        {2,2,2,2,2,2,2,2,2,2,2}, 
                        {2,4,7,5,4,2,2,2,9,11,2}, 
                        {2,4,3,1,4,2,2,2,8,8,2},
                        {2,2,2,2,2,2,2,2,2,2,2},
                        {2,2,2,2,2,2,2,15,2,2,2}}};
int state = 0;
int tileSize = 64;
PVector screenPos = new PVector(576, 220);
int maxHeight = gameState[state].length -1;
int maxWidth = gameState[state][0].length -1;
void setup(){
  size(1920, 1080);
  background(0);
  for (int i = 0; i < tiles.length; i++){
    tiles[i] = loadImage(imageNames[i]);
  }
  
}

void draw(){
  for (int i = 0; i < gameState[state].length; i++){
    for(int j = 0; j < gameState[state][i].length; j++){
      image(tiles[gameState[state][i][j]], screenPos.x+(j*tileSize), screenPos.y+(i*tileSize), tileSize, tileSize);
    }
  }
  
  fill(255,0,0);
  ellipse(screenPos.x + pLoc.x*tileSize +tileSize/2,screenPos.y + pLoc.y*tileSize +tileSize/2, tileSize*2/3, tileSize*2/3);
}

boolean playerCanMoveDir(int x, int y){
  return gameState[state][constrain((int)pLoc.y+y,0,maxHeight)][constrain((int)pLoc.x+x, 0,maxWidth)] %2==0 
          && pLoc.x+x >= 0 && pLoc.x+x <= maxWidth
          && pLoc.y+y >= 0 && pLoc.y+y <= maxHeight;
          
          
}

void keyPressed(){
  if (key == 'w' && playerCanMoveDir(0, -1)){
    pLoc.y --;
  }
  if (key == 'a' && playerCanMoveDir(-1, 0)){
    pLoc.x --;
  }
  if (key == 's' && playerCanMoveDir(0, 1)){
    pLoc.y ++;
  }
  if (key == 'd' && playerCanMoveDir(1, 0)){
    pLoc.x ++;
  }
}